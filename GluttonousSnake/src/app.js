/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/
var autoTestEnabled = autoTestEnabled || false;

var MainMenuLayer = cc.Layer.extend({
    sprite:null,
    ctor:function () {
        //////////////////////////////
        // 1. super init first
        this._super();
        
        PlayBGM();

        var size = cc.winSize;
        var winSize=cc.winSize;

        var helloLabel = new cc.LabelTTF("贪吃蛇大作战", "Arial", 38);
        // position the label on the center of the screen
        helloLabel.x = size.width / 2;
        helloLabel.y = size.height / 2 + 200;
        // add the label as a child to this layer
        this.addChild(helloLabel, 5);



        var NewItem = new cc.MenuItemImage(res.MenuButtona_png, res.MenuButtonb_png, this.onToNewGame, this);
        NewItem.x = winSize.width/2;
        NewItem.y = winSize.height/2-210;

        var subItem1 = new cc.MenuItemFont("Automated Test: Off");
        subItem1.fontSize = 18;
        var subItem2 = new cc.MenuItemFont("Automated Test: On");
        subItem2.fontSize = 18;

        var toggleAutoTestItem = new cc.MenuItemToggle(subItem1, subItem2);
        toggleAutoTestItem.setCallback(this.onToggleAutoTest, this);
        toggleAutoTestItem.x = winSize.width - toggleAutoTestItem.width / 2 - 10;
        toggleAutoTestItem.y = 20;
        //toggleAutoTestItem.
        toggleAutoTestItem.setVisible(false);
        if( autoTestEnabled )
            toggleAutoTestItem.setSelectedIndex(1);


        var menu = new cc.Menu(NewItem, toggleAutoTestItem);//pmenu is just a holder for the close button
        menu.x = 0;
        menu.y = 0;
        this.addChild(menu, 0);

        
        //要么缓存要么替图，不像creator那样
        // this.btn.loadTextures(res.box,res.black,res.box);
        // this.btn.setPosition(winSize.width/2,winSize.height/6);
        // this.btn.setTouchEnabled(true);
        // this.addChild(this.btn);


        // add "HelloWorld" splash screen"
        this.sprite = new cc.Sprite(res.MainPhoto_png);
        this.sprite.attr({
            x: size.width / 2,
            y: size.height / 2
        });
        this.addChild(this.sprite, 0);

        //this.scheduleUpdate();

        return true;
    },

    ensureLeftAligned: function(label) {
        label.anchorX = 0;
        label.anchorY = 1;
        label.textAlign = cc.TEXT_ALIGNMENT_LEFT;
    },

    onEnter:function(){
        this._super();
        cc.log("网络");
        let winSize=cc.winSize;
        var statusPostLabel = new cc.LabelTTF("Status:", "Thonburi", 12);
        this.addChild(statusPostLabel, 1);

        statusPostLabel.x = winSize.width / 10 ;
        statusPostLabel.y = winSize.height - 100;
        this.ensureLeftAligned(statusPostLabel);
        statusPostLabel.setString("Status: Send Post Request to httpbin.org with plain text");


        var responseLabel = new cc.LabelTTF("", "Thonburi", 16);
        this.addChild(responseLabel, 1);
        this.ensureLeftAligned(responseLabel);
        responseLabel.x = winSize.width / 10 ;
        responseLabel.y = winSize.height / 2;
        var xhr = cc.loader.getXMLHttpRequest();
        this.streamXHREventsToLabel(xhr, statusPostLabel, responseLabel, "POST", "sendPostForms");

        xhr.open("POST", "http://httpbin.org/post");
        //set Content-Type "application/x-www-form-urlencoded" to post form data
        //mulipart/form-data for upload
        xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
        /**
        form : {
            "a" : "hello",
            "b" : "world"
        }
        **/
        var args = "a=hello&b=world";
        xhr.send(args);
        cc.log("网络");
    },

    streamXHREventsToLabel: function( xhr, label, textbox, method, title ) {
        // Simple events
        ['loadstart', 'abort', 'error', 'load', 'loadend', 'timeout'].forEach(function (eventname) {
            xhr["on" + eventname] = function () {
                label.string += "\nEvent : " + eventname;
            };
        });

        // Special event
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status <= 207)) {
                var httpStatus = xhr.statusText;
                var response = xhr.responseText.substring(0, 100) + "...";
                cc.log("title:" + title + ", response:\n" + xhr.responseText);
                textbox.string = method + " Response (100 chars):\n";
                textbox.string += response;
                label.string += "\nStatus: Got " + method + " response! " + httpStatus;
            }
            else
            {
                cc.log("失败");
            }
        };
    },

    // update:function(dt)
    // {

    //     //由于之前场景组合时导致了两层layer，所以出现了2个默认相机
    //     let theCamera = this.parent.getDefaultCamera();

    //     let cameraPos = theCamera.getPosition3D();
    //     console.log(cameraPos);
    //     cameraPos.x += 1;
    //     cameraPos.y += 1;
    //     cameraPos.z += 1;

    //     let rotation3D = theCamera.getRotation3D();
    //     rotation3D.y -= 1;
        
    //     theCamera.setPosition3D(cameraPos);

    // },    

    onToggleAutoTest:function() {
        autoTestEnabled = !autoTestEnabled;
    },
    
    onToNewGame:function () {
        //cc.director.runScene(new NewGame());
        let scene = new NewGame();
        let transition = new cc.TransitionProgressRadialCCW(0.5,scene);
        cc.director.runScene(transition);
    },

    onCloseCallback:function () {
        if (cc.sys.isNative)
        {
            cc.game.end();
        }
        else {
            window.history && window.history.go(-1);
        }
    },
});


var PlayerHeadCollision=[];//元素为对象引用 引用蛇头
var PlayerBodyCollision=[];//元素为数组引用 引用蛇身数组
var EnemyHeadCollision=[];
var EnemyBodyCollision=[];
var BarrierCollisionLR=[];
var BarrierCollisionUD=[];
var foodArray=[];
var MagnetArray=[];
var EnemySum=10;

var BarrierScale=2.5;

var NewGameLayer = cc.Layer.extend({
    sprite:null,
    _camera:null,
    gameOver:false,
    cT:1,
    _logicCamera:null,
    _uiCamera:null,
    _enemySum:0,
    _enemyAddOrder:0,
    _enemyCurrentNum:0,
    ctor:function () {
        //////////////////////////////
        // 1. super init first
        this._super();
        //this.scheduleUpdate();//加上这一句就能启动update
        this.schedule(this.updateFood,0.1);
        this.schedule(this.updateMagnet,6.0);
        this.schedule(this.randomCreatFewEnemy,1);
        this.scheduleUpdate();
        this.cT=1;
        this._camera=null;
        this.gameOver=false;
        this._enemySum=EnemySum;
        this._enemyAddOrder=0;
        this._enemyCurrentNum=0;
        /////////////////////////////
        // 2. add a menu item with "X" image, which is clicked to quit the program
        //    you may modify it.
        // ask the window size
        var size = cc.winSize;
        var winSize=cc.winSize;
        /////////////////////////////
        // 3. add your codes below...
        // add a label shows "Hello World"
        // create and initialize a label
        let helloLabel = new cc.LabelTTF("游戏界面", "Arial", 38);
        helloLabel.x = 100;
        helloLabel.y = size.height-50;
        this.addChild(helloLabel, 55);
        helloLabel.setCameraMask(4);
        helloLabel.enableShadow(null,cc.size(2,2),1);

        this.eatLabel = new cc.LabelTTF("总吸取:10", "Arial", 38);
        this.eatLabel.x = 150;
        this.eatLabel.y = size.height-100;
        this.addChild(this.eatLabel, 56);
        this.eatLabel.setCameraMask(4);
        this.eatLabel.enableShadow(null,cc.size(2,2),1);

        this.killLabel = new cc.LabelTTF("总击杀:0", "Arial", 38);
        this.killLabel.x = 150;
        this.killLabel.y = size.height-150;
        this.addChild(this.killLabel, 57);
        this.killLabel.setCameraMask(4);
        this.killLabel.enableShadow(null,cc.size(2,2),1);

        var closeItem = new cc.MenuItemImage(res.MenuButtona_png, res.MenuButtonb_png, this.onToMainMenu, this);
        closeItem.x = winSize.width-100;
        closeItem.y = winSize.height-50;

        var subItem1 = new cc.MenuItemFont("Automated Test: Off");
        subItem1.fontSize = 18;
        var subItem2 = new cc.MenuItemFont("Automated Test: On");
        subItem2.fontSize = 18;

        var toggleAutoTestItem = new cc.MenuItemToggle(subItem1, subItem2);
        toggleAutoTestItem.setCallback(this.onToggleAutoTest, this);
        toggleAutoTestItem.x = winSize.width - toggleAutoTestItem.width / 2 - 10;
        toggleAutoTestItem.y = 20;
        //toggleAutoTestItem.
        toggleAutoTestItem.setVisible(false);
        if( autoTestEnabled )
            toggleAutoTestItem.setSelectedIndex(1);


        var menu = new cc.Menu(closeItem, toggleAutoTestItem);//pmenu is just a holder for the close button
        menu.x = 0;
        menu.y = 0;
        this.addChild(menu, 51);
        menu.setCameraMask(4);
        

        this.thePlayer=new SnakeCreat(res.SnakeHead1_png,Math.PI/4,200,200,1,0);
        this.thePlayer.x=0;
        this.thePlayer.y=0;
        this.addChild(this.thePlayer,10);
        this.thePlayer.setCameraMask(2);
        this.eatLabel.string="总吸取:"+this.thePlayer.SNBody.length;
        //PlayerHeadCollision.push(this.thePlayer.SNHead);
        //PlayerBodyCollision.push(this.thePlayer.SNBody);


        this.randomCreatSomeEnemy();

        // let theEnemy1=new SnakeCreat(res.SnakeHead1_png,3*Math.PI/4,800,200,2,0);
        // theEnemy1.x=0;
        // theEnemy1.y=0;
        // this.addChild(theEnemy1,11);
        // theEnemy1.setCameraMask(2);
        // EnemyHeadCollision.push(theEnemy1.SNHead);
        // EnemyBodyCollision.push(theEnemy1.SNBody);


        // let theEnemy2=new SnakeCreat(res.SnakeHead1_png,5*Math.PI/4,800,500,2,1);
        // theEnemy2.x=0;
        // theEnemy2.y=0;
        // this.addChild(theEnemy2,12);
        // theEnemy2.setCameraMask(2);
        // EnemyHeadCollision.push(theEnemy2.SNHead);
        // EnemyBodyCollision.push(theEnemy2.SNBody);

        // let theEnemy3=new SnakeCreat(res.SnakeHead1_png,7*Math.PI/4,200,500,2,2);
        // theEnemy3.x=0;
        // theEnemy3.y=0;
        // this.addChild(theEnemy3,12);
        // theEnemy3.setCameraMask(2);
        // EnemyHeadCollision.push(theEnemy3.SNHead);
        // EnemyBodyCollision.push(theEnemy3.SNBody);

        // let theEnemy4=new SnakeCreat(res.SnakeHead1_png,5*Math.PI/4,-100,600,2,3);
        // theEnemy4.x=0;
        // theEnemy4.y=0;
        // this.addChild(theEnemy4,12);
        // theEnemy4.setCameraMask(2);
        // EnemyHeadCollision.push(theEnemy4.SNHead);
        // EnemyBodyCollision.push(theEnemy4.SNBody);




        this.joystick = new Joystick(res.ControlBackGround_png,   
            res.ControlContent_png,   
            50,   
            TouchType.DEFAULT,   
            DirectionType.ALL,   
            this.thePlayer);  
            this.joystick.setPosition(cc.p(100, 100));  
            this.joystick.setSpeedwithLevel1(1);  
            this.joystick.setSpeedwithLevel2(2);  
        this.joystick.setOpacity(128);  
        //this.joystick.setEnable(true);  
        this.joystick.callback = this.onCallback.bind(this);  
        this.addChild(this.joystick, 80, 101);
        this.joystick.setCameraMask(4);



        this.speedButton = new SpeedButton(res.ControlBackGround_png,null,this.thePlayer);  
        this.speedButton.setPosition(cc.p(size.width-100, 100));  
        //this.joystick.setOpacity(128);  
        //this.joystick.setEnable(true);  
        //this.joystick.callback = this.onCallback.bind(this);  
        this.addChild(this.speedButton, 81, 102);  
        this.speedButton.setCameraMask(4);
        


        //右
        this.MarginBarrier1=new MarginBarrierLR(res.wall_png,cc.rect(0, 0, size.width, 2*BarrierScale*size.height));//cc.rect(100, 100, 100, 100)
        this.MarginBarrier1.x=BarrierScale*size.width;
        this.MarginBarrier1.y=size.height/2;
        this.addChild(this.MarginBarrier1,1); 
        this.MarginBarrier1.setCameraMask(2);
        BarrierCollisionLR.push(this.MarginBarrier1);
        //this.goodGame.setCameraMask();

        //左
        this.MarginBarrier2=new MarginBarrierLR(res.wall_png,cc.rect(0, 0, size.width, 2*BarrierScale*size.height));//cc.rect(100, 100, 100, 100)
        this.MarginBarrier2.x=-BarrierScale*size.width;
        this.MarginBarrier2.y=size.height/2;
        this.addChild(this.MarginBarrier2,2); 
        this.MarginBarrier2.setCameraMask(2);
        BarrierCollisionLR.push(this.MarginBarrier2);

        //上
        this.MarginBarrier3=new MarginBarrierUD(res.wall_png,cc.rect(0, 0, 2*BarrierScale*size.width, size.height));//cc.rect(100, 100, 100, 100)
        this.MarginBarrier3.x=size.width/2;
        this.MarginBarrier3.y=BarrierScale*size.height;
        this.addChild(this.MarginBarrier3,3); 
        this.MarginBarrier3.setCameraMask(2);
        BarrierCollisionUD.push(this.MarginBarrier3);

        //下
        this.MarginBarrier4=new MarginBarrierUD(res.wall_png,cc.rect(0, 0, 3*BarrierScale*size.width, size.height));//cc.rect(100, 100, 100, 100)
        this.MarginBarrier4.x=size.width/2;
        this.MarginBarrier4.y=-BarrierScale*size.height;
        this.addChild(this.MarginBarrier4,4); 
        this.MarginBarrier4.setCameraMask(2);
        BarrierCollisionUD.push(this.MarginBarrier4);
        // this._camera = new cc.Camera(cc.Camera.Mode.PERSPECTIVE, 60, size.width/size.height, 1, 500); 
        // this._camera.x+=100;
        // this._camera.setCameraFlag(cc.CameraFlag.USER1);
        // this.addChild(this._camera);
        //;


        this.goodGame = new cc.LabelTTF("GG", "Arial", 98);
        this.goodGame.x = size.width/2;
        this.goodGame.y = size.height/2;
        this.goodGame.setVisible(false);
        this.addChild(this.goodGame, 100);
        this.goodGame.setCameraMask(4);

        this._logicCamera=new cc.Camera(cc.Camera.Mode.PERSPECTIVE, 10000, size.width/size.height, 1, 5000);
        //this._logicCamera.initPerspective(60,size.width/size.height,1,0);
        this._logicCamera.initDefault();
        this.addChild(this._logicCamera);
        //this._logicCamera.setDepth(1);多个相机间的层级关系
        console.log("camera",this._logicCamera.getDepth());

        this._uiCamera=new cc.Camera(cc.Camera.Mode.PERSPECTIVE, 1000, size.width/size.height, 1, 500);
        this._uiCamera.initDefault();
        //this._uiCamera.
        this.addChild(this._uiCamera);
        

        return true;
    },

    
 
    
    onEnter:function()
    {


        let size=cc.winSize;
        this._super();
        this._camera = this.parent.getDefaultCamera();

        this._camera.setCameraFlag(8);
        
        // this._camera.x=size.width/2;
        // this._camera.y=size.height/2;
        // this._camera.lookAt(cc.math.vec3(0, 0, 0),cc.math.vec3(0, 0, 0));

        this._logicCamera.setCameraFlag(2);
        this._logicCamera.x=size.width/2;
        this._logicCamera.y=size.height/2;
        this._logicCamera.lookAt(cc.math.vec3(0, 0, 0),cc.math.vec3(0, 0, 0));   
        let currentPos=this._logicCamera.getPosition3D();
        currentPos.z-=150;
        this._logicCamera.setPosition3D(currentPos);

        this._uiCamera.setCameraFlag(4);
        this._uiCamera.x=size.width/2;
        this._uiCamera.y=size.height/2;
        this._uiCamera.lookAt(cc.math.vec3(0, 0, 0),cc.math.vec3(0, 0, 0));       
        // console.log("NewCamera",this._uiCamera.getCameraFlag());
        // console.log(this._camera.getViewProjectionMatrix());
        // console.log(this._uiCamera.getViewProjectionMatrix());

        // var keys1 = [];
        // var keys2 = [];
        // for (var property in this._uiCamera)
        // {
        //     keys1.push(property);
        //     keys1.push('\n');
        //     keys2.push(property);
        // }
        // //keys1
        // console.log(keys1);

        //console.log(Object.keys(this._camera));

        // let size=cc.winSize;
        // this._uiCamera=new cc.Camera(cc.Camera.Mode.PERSPECTIVE, 1000, size.width/size.height, 1, 500);
        // this._uiCamera.setCameraFlag(3);
        // this._uiCamera.x=-480;
        // this._uiCamera.y=-320;
        // this._uiCamera.z=-554.256;
        // this._uiCamera.lookAt(cc.math.vec3(0, 0, 0));
        // console.log(this._uiCamera.getCameraFlag());






    },


    randomCreatSomeEnemy:function()
    {
        for(let i=0;i<this._enemySum;++i)
        {
            let size=cc.winSize;
            let randomX=Math.random()*(2*(BarrierScale-0.5)*size.width-400)-(BarrierScale-0.5)*size.width+200;
            let randomY=Math.random()*(2*(BarrierScale-0.5)*size.height-400)-(BarrierScale-0.5)*size.height+200;
            this.creatEnemy(Math.PI/4,randomX,randomY);
        }
    },

    randomCreatFewEnemy:function()
    {
        if(this.gameOver==true)
        {
            return;
        }
        for(let i=this._enemyCurrentNum;i<this._enemySum;++i)
        {
            let size=cc.winSize;
            let randomX=Math.random()*(2*(BarrierScale-0.5)*size.width-400)-(BarrierScale-0.5)*size.width+200;
            let randomY=Math.random()*(2*(BarrierScale-0.5)*size.height-400)-(BarrierScale-0.5)*size.height+200;
            this.creatEnemy(Math.PI/4,randomX,randomY);
        }
        //this.scheduleOnce(this.randomCreatEnemy,0.2);
        //this.creatEnemy(Math.PI/4,randomX,randomY);
    },

    creatEnemy:function(theDirection,theX,theY)
    {
        let theEnemy1=new SnakeCreat(res.SnakeHead1_png,theDirection,theX,theY,2,this._enemyAddOrder);
        ++this._enemyAddOrder;
        theEnemy1.x=0;
        theEnemy1.y=0;
        this.addChild(theEnemy1,11);
        theEnemy1.setCameraMask(2);

        ++this._enemyCurrentNum;
    },

    // randomCreatEnemy:function()
    // {
    //     let randomX=Math.floor(Math.random(cc.winSize.width));
    //     let randomY=Math.floor(Math.random(cc.winSize.height));

    //     let theEnemy1=new SnakeCreat(res.SnakeHead1_png,Math.PI/4,randomX,randomY,2,this._enemyAddOrder);
    //     ++this._enemyAddOrder;
    //     theEnemy1.x=0;
    //     theEnemy1.y=0;
    //     this.addChild(theEnemy1,11);
    //     theEnemy1.setCameraMask(2);
    //     EnemyHeadCollision.push(theEnemy1.SNHead);
    //     EnemyBodyCollision.push(theEnemy1.SNBody);
    //     ++this._enemyCurrentNum;
    // },

    update:function(dt)
    {
        //console.log(this._uiCamera.getRenderOrder());

        //console.log(this._enemyCurrentNum);
        if(this.gameOver==true)
            return;
        this._logicCamera.x=this.thePlayer.SNHead.x;
        this._logicCamera.y=this.thePlayer.SNHead.y;


        //this._logicCamera.z-=1;


        // this._logicCamera.x+=1;
        // this._logicCamera.y+=1;
        // this._logicCamera.z+=1;



        // let cameraPos = this._camera.getPosition3D();
        // this._camera.setCameraFlag(1);
        // console.log(this._camera.getCameraFlag());
        // cameraPos.x = this.thePlayer.SNHead.x;
        // cameraPos.y = this.thePlayer.SNHead.y;
        // //cameraPos.z += 1;

        // let rotation3D = this._camera.getRotation3D();
        // rotation3D.y -= 1;
        
        // this._camera.setPosition3D(cameraPos);


    },    
    updateFood:function()
    {


        //tC[12]+=1;
        //tC[13]+=1;

        //theCamera.setAdditionalProjection(tC);
        
        //console.log(tC[12],tC[13]);

        //console.log(tC);

        if(this.gameOver==true)
            return;
        this.creatSomeFood();
        this.creatSomeFood();

    },
    creatSomeFood:function()
    {
        let size = cc.winSize;
        let currentFood=new foods(res.Food_png);//cc.rect(100, 100, 100, 100)
        //currentFood.x=Math.random()*(size.width-200)+100;
        currentFood.x=Math.random()*(2*(BarrierScale-0.5)*size.width-200)-(BarrierScale-0.5)*size.width+100;
        currentFood.y=Math.random()*(2*(BarrierScale-0.5)*size.height-200)-(BarrierScale-0.5)*size.height+100;
        currentFood.scale=0.2;
        this.addChild(currentFood,0); 
        currentFood.setCameraMask(2);
        foodArray.push(currentFood);
    },

    creatDieFood:function(theX,theY)
    {
        let size = cc.winSize;
        let currentFood=new foods(res.Food_png);//cc.rect(100, 100, 100, 100)
        currentFood.x=theX;
        currentFood.y=theY;
        currentFood.scale=0.2;
        this.addChild(currentFood,0); 
        currentFood.setCameraMask(2);
        foodArray.push(currentFood);
    },

    updateMagnet:function()
    {
        if(this.gameOver==true)
            return;
        let size = cc.winSize;
        let currentMagnet=new Magnet(res.Magnet_png);//cc.rect(100, 100, 100, 100)
        currentMagnet.x=Math.random()*(2*(BarrierScale-0.5)*size.width-200)-(BarrierScale-0.5)*size.width+100;
        currentMagnet.y=Math.random()*(2*(BarrierScale-0.5)*size.height-200)-(BarrierScale-0.5)*size.height+100;
        currentMagnet.scale=0.5;
        this.addChild(currentMagnet,0); 
        currentMagnet.setCameraMask(2);
        MagnetArray.push(currentMagnet);
    },


    onCallback:function(){

    },
    onToggleAutoTest:function() {
        autoTestEnabled = !autoTestEnabled;
    },
    onToMainMenu:function () {
        //cc.director.runScene(new MainMenu());
        this.gameOver=true;
        cc.director.resume();
        //console.log(BarrierCollisionLR.length);
        PlayerHeadCollision.splice(0,PlayerHeadCollision.length);
        PlayerBodyCollision.splice(0,PlayerBodyCollision.length);
        EnemyHeadCollision.splice(0,EnemyHeadCollision.length);
        EnemyBodyCollision.splice(0,EnemyBodyCollision.length);
        BarrierCollisionLR.splice(0,BarrierCollisionLR.length);
        BarrierCollisionUD.splice(0,BarrierCollisionUD.length);
        foodArray.splice(0,foodArray.length);
        MagnetArray.splice(0,MagnetArray.length);

        //console.log(BarrierCollisionLR.length);      

        let scene = new cc.Scene();
        let layer = new MainMenu();
        scene.addChild(layer);
        let transition = new cc.TransitionProgressRadialCCW(0.5,scene);
        cc.director.runScene(transition);
    },
    onCloseCallback:function () {
        if (cc.sys.isNative)
        {
            cc.game.end();
        }
        else {
            window.history && window.history.go(-1);
        }
    },
});


 




// var PlayerSnake = cc.Sprite.extend({
// 	velocity: null, // 移动速度
// 	radius: 25,	    // 碰撞半径
//     curScene: null, // 游戏场景的引用
// 	ctor: function(scene) {
// 		this._super();
//                 this.curScene = scene;
// 		this.initWithFile(res.SnakeHead1_png);	// 赋予贴图
// 		this.initData();    // 初始化数据
// 	}
// });


var HelloWorldScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new HelloWorldLayer();
        this.addChild(layer);
    }
});

var MainMenu = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new MainMenuLayer();
        this.addChild(layer);
    }
});

var NewGame = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new NewGameLayer();
        this.addChild(layer);
    }
});
