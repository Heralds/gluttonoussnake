/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

var res = {
    HelloWorld_png : "res/HelloWorld.png",
    MainPhoto_png : "res/MainPhoto.png",
    MenuButton_png : "res/MenuButton.png",
    MenuButtona_png : "res/MenuButtona.png",
    MenuButtonb_png : "res/MenuButtonb.png",
    SnakeHead1_png : "res/SnakeHead1.png",
    SnakeBody1_png : "res/SnakeBody1.png",
    ControlContent_png : "res/ControlContent.png",
    ControlBackGround_png : "res/ControlBackGround.png",
    wall_png:"res/wall.png",
    Food_png:"res/food.png",
    Magnet_png:"res/Magnet.png",
    Shell_png:"res/shell.png",

    BGM_1_mp3:"res/BGM_1.mp3",
    Eat1_mp3:"res/Eat1.mp3",
    Eat2_mp3:"res/Eat2.mp3",

    EatFood1_mp3:"res/EatFood1.mp3",
    EatFood2_mp3:"res/EatFood2.mp3"
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
