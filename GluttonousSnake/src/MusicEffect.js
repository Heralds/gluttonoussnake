
var EffectVolume=0.7;
var BGMVolume=0.5;

var PlayBGM=function(){
    if(cc.audioEngine.isMusicPlaying()==false)
    {
        cc.audioEngine.playMusic(res.BGM_1_mp3,true);
        cc.audioEngine.setMusicVolume(BGMVolume);
    }
}

var PlayEatEffect=function(){
    let randomAudio=Math.floor(Math.random()*2+1);
    if(randomAudio==1)
    {
        cc.audioEngine.playEffect(res.Eat1_mp3,false);
        cc.audioEngine.setEffectsVolume(EffectVolume);
    }
    else
    {
        cc.audioEngine.playEffect(res.Eat2_mp3,false);
        cc.audioEngine.setEffectsVolume(EffectVolume);                            
    }
}

var PlayEatFoodEffect=function()
{
    //if(cc.audioEngine.)
    let randomAudio=Math.floor(Math.random()*2+1);
    if(randomAudio==1)
    {
        cc.audioEngine.playEffect(res.EatFood1_mp3,false);
        cc.audioEngine.setEffectsVolume(EffectVolume*0.5);
    }
    else
    {
        cc.audioEngine.playEffect(res.EatFood2_mp3,false);
        cc.audioEngine.setEffectsVolume(EffectVolume*0.5);                            
    }
}