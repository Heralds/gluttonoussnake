
var rotationSpeed=8;
var PlayerSpeed=150;
var PreventRotation=4;
var MinDistance=0;
var MinAbsorbRadius=10;
var MaxAbsorbRadius=100;
var AbsorbTime=5;
var AbsorbSpeed=8;

var BeginLength=30;
var BeginBigSize=0.15;
var BeginRadius=10;

var scaleImprove=0.0025;
var bigSizeImprove=0.0025;
var radiusImprove=0.125;
var snakeSpeedImprove=1.5;
var cameraDistanceImprove=2.5;


var SnakeCreat = cc.Sprite.extend({
    SNHead:null,
    SNBody:[],
    snakeDirection: 0.78539,
    snakeSpeed: 150,
    speedUpState: false,
    magneticState: false,
    dieState: false,
    invincibleState:false,
    sumLong: 5,//加上头是5个
    radius:10,
    bigSize:0.15,
    childLayer:10000,
    _image:null,
    _theX:0,
    _theY:0,
    _theType:0,
    _orderInArray:0,
    _ComputerAI:null,
    _killSum:0,
    _theShell:null,
    ctor:function (image,toWhere,theX,theY,theType,orderInArray) {
        this._super();//filename
        this.SNHead=null;
        this.SNBody=[];
        this.snakeDirection= toWhere;
 
        this._theX=theX;
        this._theY=theY;
        this._theType=theType;
        this._orderInArray=orderInArray;
        this.snakeSpeed= PlayerSpeed;
        this.speedUpState= false;
        this.magneticState= false;
        this.dieState= false;
        this.invincibleState=true;
        this.sumLong= BeginLength;//加上头是6个
        this.radius=BeginRadius;
        this.bigSize=BeginBigSize;
        this.childLayer=10000,

        this._image=image;

        this._killSum=0;
        this._theShell=null;
        this.initLength(image,toWhere);
        this.scheduleUpdate();//加上这一句就能启动update
        this.scheduleOnce(this.overInvincible,2.5);
        //this.scheduleOnce(this.posAjustment,1);

    },

    initLength:function(image,toWhere)
    {
        let creatHead=new SnakeHead(image,toWhere);
        creatHead.x=this.x+this._theX;
        creatHead.y=this.y+this._theY;
        creatHead.setScale(this.bigSize,this.bigSize);
        this.addChild(creatHead,this.childLayer);
        creatHead.setCameraMask(2);

        --this.childLayer;
        this.SNHead=creatHead;
        this.SNHead.prev=this;
        if(this._theType==2)
        {
            creatHead.color=cc.color(255,155,155,255);
        }


        for(let i=0;i<this.sumLong;++i)//加上头是六个
        {              
            let creatBody=new SnakeBody(image,toWhere,this);
            creatBody.setScale(this.bigSize,this.bigSize);
            if(this._theType==2)
            {
                creatBody.color=cc.color(255,155,155,255);
            }

            let bodylength=this.SNBody.length;
            if(bodylength<=0)
            {
                this.SNHead.next=creatBody;
                creatBody.prev=this.SNHead;
            }
            else
            {
                this.SNBody[bodylength-1].next=creatBody;
                creatBody.prev=this.SNBody[bodylength-1];
            } 
            //console.log(Math.cos(Math.PI/4),Math.sin(Math.PI/4));
           
            let theNum=bodylength+1;
            creatBody.x=this.x+this._theX-theNum*PlayerSpeed/60*Math.cos(this.snakeDirection);//2.3
            creatBody.y=this.y+this._theY-theNum*PlayerSpeed/60*Math.sin(this.snakeDirection);
            this.addChild(creatBody,this.childLayer);
            creatBody.setCameraMask(2);
            --this.childLayer;
            this.SNBody.push(creatBody);
        }

        

        

        if(this._theType!=1)
        {
            console.log("获得了AI");
            this._ComputerAI=new ComputerAI(this);
            this.addChild(this._ComputerAI,-1);
        }
        //console.log(this.SNBody.length);
    },

    onEnter:function()
    {
        this._super();
        this.addShell();
    },

    addShell:function()
    {
        this._theShell=new cc.Sprite(res.Shell_png);
        //theShell.x=0;
        //theShell.y=0;
        //console.log(theShell.getAnchorPoint().x);
        //theShell.setAnchorPoint(1, 1);
        //theShell.setScale(this.bigSize,this.bigSize);
        this.SNBody[this.SNBody.length/2].addChild(this._theShell,10010);
        this._theShell.setScale(5,5);
        this._theShell.x=65;
        this._theShell.y=60;
        this._theShell.setCameraMask(2);
        console.log( this.SNBody[this.SNBody.length/2].getAnchorPoint().x, this.SNBody[this.SNBody.length/2].getAnchorPoint().y);        
        console.log(this._theShell.getAnchorPoint().x,this._theShell.getAnchorPoint().y);        
        
        console.log( this.SNBody[this.SNBody.length/2].convertToWorldSpaceAR().x, this.SNBody[this.SNBody.length/2].convertToWorldSpaceAR().y);        
        console.log(this._theShell.convertToWorldSpaceAR().x,this._theShell.convertToWorldSpaceAR().y);
    },

    posAjustment:function()
    {
        this._theShell.x=65;
        this._theShell.y=60;
    },


    update:function(dt)
    {

        // this.SNHead.x+=this.snakeSpeed*dt*Math.cos(this.snakeDirection);
        // this.SNHead.y+=this.snakeSpeed*dt*Math.sin(this.snakeDirection);
        
        this.onceUpdate(dt);
        if(this.speedUpState==true)
            this.onceUpdate(dt);
    },

    overInvincible:function()
    {
        if(this.parent.gameOver==true)
        {  
            return;
        }
        
        this.invincibleState=false;
        this._theShell.parent.removeChild(this._theShell);
        if(this._theType==1)
        {
            PlayerHeadCollision.push(this.SNHead);
            PlayerBodyCollision.push(this.SNBody);
        }
        else
        {
            EnemyHeadCollision.push(this.SNHead);
            EnemyBodyCollision.push(this.SNBody);
        }
    },

    onceUpdate:function(dt)
    {
        this.SNHead.oldDirection=this.SNHead.rotation;
        this.SNHead.oldPosX=this.SNHead.x;//-MinDistance*Math.cos(-(this.SNHead.oldDirection-90)*(Math.PI/180));
        this.SNHead.oldPosY=this.SNHead.y;//-MinDistance*Math.sin(-(this.SNHead.oldDirection-90)*(Math.PI/180));
        
        this.rotationChange(dt);
        
        this.SNHead.x+=this.snakeSpeed*dt*Math.cos(-(this.SNHead.rotation-90)*(Math.PI/180));
        this.SNHead.y+=this.snakeSpeed*dt*Math.sin(-(this.SNHead.rotation-90)*(Math.PI/180));

        for(let i=0;i<this.SNBody.length;++i)
        {
            this.SNBody[i].upDate(dt);
            //console.log(this.SNBody[i].prev.oldPos.x,this.SNBody[i].prev.oldPos.y);
        }
    },

    addNewBody:function(image,toWhere)
    {
        let creatBody=new SnakeBody(image,toWhere,this);
        creatBody.setScale(this.bigSize,this.bigSize);
        if(this._theType==2)
        {
            creatBody.color=cc.color(255,155,155,255);
        }
        let bodylength=this.SNBody.length;
        if(bodylength<=0)
        {
            this.SNHead.next=creatBody;
            creatBody.prev=this.SNHead;
        }
        else
        {
            this.SNBody[bodylength-1].next=creatBody;
            creatBody.prev=this.SNBody[bodylength-1];
        }
        let theNum=bodylength+1;
        creatBody.x=this.SNBody[bodylength-1].x;
        creatBody.y=this.SNBody[bodylength-1].y;
        this.addChild(creatBody,this.childLayer);
        creatBody.setCameraMask(2);
        --this.childLayer;
        this.SNBody.push(creatBody);
    },

    rotationChange:function(dt)
    {
        let currentDirection= 90-this.snakeDirection/(Math.PI/180);
        
        if(this.SNHead.rotation>360)
        {
            this.SNHead.rotation=this.SNHead.rotation%360;
        }
        else if(this.SNHead.rotation<0)
        {
            this.SNHead.rotation=360+this.SNHead.rotation;
        }
        //console.log(currentDirection,this.SNHead.rotation);
        if(currentDirection>=0)
        {
            if(Math.abs(currentDirection-this.SNHead.rotation)<PreventRotation)
                return;
            if(currentDirection>this.SNHead.rotation)
            {
                if(currentDirection-this.SNHead.rotation>180)
                {
                    this.SNHead.rotation-=rotationSpeed;
                }
                else
                {
                    this.SNHead.rotation+=rotationSpeed;
                }
            }
            else
            {
                if(this.SNHead.rotation-currentDirection>180)
                {
                    this.SNHead.rotation+=rotationSpeed;
                }
                else
                {
                    this.SNHead.rotation-=rotationSpeed;
                }
            }
        }
        else
        {
            if(Math.abs(360+currentDirection-this.SNHead.rotation)<PreventRotation)
                return;
            if(360+currentDirection>this.SNHead.rotation)
            {
                if(360+currentDirection-this.SNHead.rotation>180)
                {
                    this.SNHead.rotation-=rotationSpeed;
                }
                else
                {
                    this.SNHead.rotation+=rotationSpeed;
                }
            }
            else
            {
                if(this.SNHead.rotation-(360+currentDirection)>180)
                {
                    this.SNHead.rotation+=rotationSpeed;
                }
                else
                {
                    this.SNHead.rotation-=rotationSpeed;
                }
            }
        }

    },

    speedUp:function()
    {
        //this.snakeSpeed*=2;
        this.speedUpState=true;
        //this.teleportAction();
    },

    slowDown:function()
    {
        //this.snakeSpeed/=2;
        this.speedUpState=false;
    },




});

var SnakeHead=cc.Sprite.extend({
    prev : null,
    next : null,
    oldPosX:0,
    oldPosY:0,
    oldDirection:1,
    snakeDirection:0.78539,
    snakeSpeedH:150,
    radius:10,
    _image:null,
    AbsorbRadius:10,
    nowSecond:0,
    magneticTime:0,
    ctor:function (image,toWhere) {
        this._super(image);
        this.snakeDirection= toWhere;
        this.snakeSpeedH= PlayerSpeed;
        this.radius=BeginRadius;
        //出始方向
        this.rotation=90-this.snakeDirection*(180/Math.PI);

        this.oldDirection=this.rotation;
        this.oldPosX=this.x;
        this.oldPosY=this.y;
 
        this.nowSecond=0;
        this.AbsorbRadius=MinAbsorbRadius;
        this.magneticTime=0;
        this.schedule(this.oneSecondUpdate,1);
        this.scheduleUpdate();//加上这一句就能启动update

        this._image=image;
        
    },

    update:function(dt)
    {

        this.snakeDirection=this.prev.snakeDirection;

        //防止场景切换前的短暂帧执行
        if(this.parent.parent.gameOver==true)
        {  
            return;
        }

        if(this.dieLogicByWall()==true)
        {
            return;
        }
 
        if(this.dieLogicBySnake()==true)
        {
            return;
        }       
        //console.log(Math.atan2(10,10));
        
        if(this.parent.parent.dieState==true)
            return;

        this.eatFoodFunc(dt);
        this.eatMagnet(dt);



    },

    dieLogicByWall:function(){
        let size=cc.winSize;
        //size.width/2 是障碍物的厚度
        if(this.x+this.radius>BarrierCollisionLR[0].x-size.width/2||this.x-this.radius<BarrierCollisionLR[1].x+size.width/2
            ||this.y+this.radius>BarrierCollisionUD[0].y-size.height/2||this.y-this.radius<BarrierCollisionUD[1].y+size.height/2)
        { 
            if(this.parent._theType==1)
            {
                this.parent.parent.goodGame.setVisible(true);
                console.log("GG");
                for(let i=0;i<PlayerHeadCollision.length;++i)
                {
                    if(PlayerHeadCollision[i].parent._orderInArray==this.parent._orderInArray)
                    {   
                        PlayEatEffect();
                        PlayerHeadCollision.splice(i,1);
                        PlayerBodyCollision.splice(i,1);  
                        break;
                    }
                }
                // for(let i=0;i<PlayerBodyCollision.length;++i)
                // {
                //     if(PlayerBodyCollision[i][0]._master._orderInArray==this.parent._orderInArray)
                //     {
                //         PlayerBodyCollision.splice(i,1);  
                //         break;
                //     }
                // }
     
                this.creatDieFood();
                this.parent.parent.gameOver=true;
                cc.director.pause();
            }
            else
            {
                for(let i=0;i<EnemyHeadCollision.length;++i)
                {
                    if(EnemyHeadCollision[i].parent._orderInArray==this.parent._orderInArray)
                    {
                        --this.parent.parent._enemyCurrentNum;
                        console.log(this.parent.parent._enemyCurrentNum);
                        //this.parent.parent.randomCreatTheEnemy();
                        EnemyHeadCollision.splice(i,1);
                        EnemyBodyCollision.splice(i,1);
                        break;
                    }
                }
                // for(let i=0;i<EnemyBodyCollision.length;++i)
                // {
                //     if(EnemyBodyCollision[i][0]._master._orderInArray==this.parent._orderInArray)
                //     {
                //         EnemyBodyCollision.splice(i,1);
                //         break;
                //     }  
                // }

            }
            this.creatDieFood();
            this.parent.dieState=true;
            this.parent.parent.removeChild(this.parent);
            return true;
        }
        return false;
    },

    dieLogicBySnake:function(){
        if(this.parent.invincibleState==true)
        {
            //console.log("无敌");
            return;
        }
        if(this.parent._theType==1)
        {
            for(let i=0;i<EnemyBodyCollision.length;++i)
            {
                for(let j=0;j<EnemyBodyCollision[i].length;++j)
                {

                    if(EnemyBodyCollision[i][j]!=undefined&&EnemyBodyCollision[i][j]!=null
                    &&Math.abs(this.x-EnemyBodyCollision[i][j].x)<this.radius+EnemyBodyCollision[i][j].radius
                    &&Math.abs(this.y-EnemyBodyCollision[i][j].y)<this.radius+EnemyBodyCollision[i][j].radius)
                    { 
                        this.parent.parent.goodGame.setVisible(true);
                        console.log("GG");
                        this.creatDieFood();
                        this.parent.parent.gameOver=true;
                        cc.director.pause();
                        
                        this.parent.dieState=true;
                        for(let k=0;k<PlayerHeadCollision.length;++k)
                        {
                            if(PlayerHeadCollision[k].parent._orderInArray==this.parent._orderInArray)
                            {   
                                PlayEatEffect();
                                PlayerHeadCollision.splice(k,1);
                                PlayerBodyCollision.splice(k,1);  
                                break;
                            }
                        }
                        this.parent.parent.removeChild(this.parent);
                        return true;
                    }
                }
            }
            return false;
        }
        else
        {
            for(let i=0;i<PlayerBodyCollision.length;++i)
            {
                for(let j=0;j<PlayerBodyCollision[i].length;++j)
                {
                    if(Math.abs(this.x-PlayerBodyCollision[i][j].x)<this.radius+PlayerBodyCollision[i][j].radius
                    &&Math.abs(this.y-PlayerBodyCollision[i][j].y)<this.radius+PlayerBodyCollision[i][j].radius)
                    { 
                        this.creatDieFood();
                        this.parent.dieState=true;
                        PlayerHeadCollision[0].parent._killSum+=1;
                        this.parent.parent.killLabel.string="总击杀:"+PlayerHeadCollision[0].parent._killSum;
                        for(let k=0;k<EnemyHeadCollision.length;++k)
                        {
                            if(EnemyHeadCollision[k].parent._orderInArray==this.parent._orderInArray)
                            {   
                                --this.parent.parent._enemyCurrentNum;
                                console.log(this.parent.parent._enemyCurrentNum);
                                //this.parent.parent.randomCreatTheEnemy();
                                EnemyHeadCollision.splice(k,1);
                                EnemyBodyCollision.splice(k,1);  
                                break;
                            }
                        }
                        this.parent.parent.removeChild(this.parent);
                        return true;
                    }
                }
            }
            
            for(let i=0;i<EnemyBodyCollision.length;++i)
            {
                if(this.parent._orderInArray==EnemyBodyCollision[i][0]._master._orderInArray)
                {
                    continue;
                }
                for(let j=0;j<EnemyBodyCollision[i].length;++j)
                {
                    //EnemyBodyCollision[i]!=undefined&&EnemyBodyCollision[i]!=null
                    if(EnemyBodyCollision[i][j]!=undefined&&EnemyBodyCollision[i][j]!=null
                    &&Math.abs(this.x-EnemyBodyCollision[i][j].x)<this.radius+EnemyBodyCollision[i][j].radius
                    &&Math.abs(this.y-EnemyBodyCollision[i][j].y)<this.radius+EnemyBodyCollision[i][j].radius)
                    { 
                        this.creatDieFood();
                        this.parent.dieState=true;
                        for(let k=0;k<EnemyHeadCollision.length;++k)
                        {
                            if(EnemyHeadCollision[k].parent._orderInArray==this.parent._orderInArray)
                            {  
                                --this.parent.parent._enemyCurrentNum;
                                console.log(this.parent.parent._enemyCurrentNum); 
                                //this.parent.parent.randomCreatTheEnemy();
                                EnemyHeadCollision.splice(k,1);
                                EnemyBodyCollision.splice(k,1);  
                                break;
                            }
                        }
                        this.parent.parent.removeChild(this.parent);
                        return true;
                    }
                }
            }

            return false;          
        }

    },

    creatDieFood:function(){
        for(let i=0;i<this.parent.SNBody.length;++i)
        {
            if(Math.floor(Math.random()*3+1)==2)
            {
                this.parent.parent.creatDieFood(this.parent.SNBody[i].x,this.parent.SNBody[i].y);
            }
        }

    },

    eatFoodFunc:function(dt){
        for(let i=0;i<foodArray.length;++i)
        {
            if(Math.abs(this.x-foodArray[i].x)<this.radius+foodArray[i].radius+this.AbsorbRadius
            &&Math.abs(this.y-foodArray[i].y)<this.radius+foodArray[i].radius+this.AbsorbRadius)
            {
                // let foodToHeadAngle=Math.atan2((this.y-foodArray[i].y),(this.x-foodArray[i].x))* (180/cc.PI);
                // //console.log(foodToHeadAngle);
                // let currenTan=(this.y-foodArray[i].y)/(this.x-foodArray[i].x);
                foodArray[i].x+=dt*AbsorbSpeed*(this.x-foodArray[i].x);
                foodArray[i].y+=dt*AbsorbSpeed*(this.y-foodArray[i].y);
            }
            // foodArray[i].x+=dt*400*Math.cos(foodToHeadAngle);
            // foodArray[i].y+=dt*400*Math.sin(foodToHeadAngle);            
            
            if(Math.abs(this.x-foodArray[i].x)<this.radius+foodArray[i].radius
            &&Math.abs(this.y-foodArray[i].y)<this.radius+foodArray[i].radius)
            {
                foodArray[i].parent.removeChild(foodArray[i]);
                foodArray.splice(i,1);
                this.toBig();

                this.parent.addNewBody(this._image,this.parent.SNBody[this.parent.SNBody.length-1].snakeDirection);
                if(this.parent._theType==1)
                {
                    PlayEatFoodEffect();
                    this.parent.parent.eatLabel.string="总吸取:"+this.parent.SNBody.length;
                    this.cameraFurther();


                }
            }
        }
    },

    cameraFurther:function()
    {
        let currentPos=this.parent.parent._logicCamera.getPosition3D();
        currentPos.z+=cameraDistanceImprove;
        if(currentPos.z<=850)
        {
            this.parent.parent._logicCamera.setPosition3D(currentPos);
        }
    },

    eatMagnet:function(dt){
        for(let i=0;i<MagnetArray.length;++i)
        {
            if(Math.abs(this.x-MagnetArray[i].x)<this.radius+MagnetArray[i].radius+this.AbsorbRadius
            &&Math.abs(this.y-MagnetArray[i].y)<this.radius+MagnetArray[i].radius+this.AbsorbRadius)
            {
                // let foodToHeadAngle=Math.atan2((this.y-foodArray[i].y),(this.x-foodArray[i].x))* (180/cc.PI);
                // //console.log(foodToHeadAngle);
                // let currenTan=(this.y-foodArray[i].y)/(this.x-foodArray[i].x);
                MagnetArray[i].x+=dt*AbsorbSpeed*(this.x-MagnetArray[i].x);
                MagnetArray[i].y+=dt*AbsorbSpeed*(this.y-MagnetArray[i].y);
            }

            if(Math.abs(this.x-MagnetArray[i].x)<this.radius+MagnetArray[i].radius
            &&Math.abs(this.y-MagnetArray[i].y)<this.radius+MagnetArray[i].radius)
            {
                this.addAbsorbRadius();
                MagnetArray[i].parent.removeChild(MagnetArray[i]);
                MagnetArray.splice(i,1);
            }

        }
    },

    toBig:function(){
        this.scale+=scaleImprove;
        this.parent.bigSize+=bigSizeImprove;
        this.parent.radius+=radiusImprove;
        this.radius+=radiusImprove;
        this.parent.snakeSpeed+=snakeSpeedImprove;
        for(let i=0;i<this.parent.SNBody.length;++i)
        {
            this.parent.SNBody[i].scale+=bigSizeImprove;
            this.parent.SNBody[i].radius+=radiusImprove;            
        }

    },

    addAbsorbRadius:function(){
        this.parent.magneticState=true;
        this.magneticTime=0;
        this.AbsorbRadius=MaxAbsorbRadius;
    },

    reduceAbsorbRadius:function(){
        this.parent.magneticState=false;
        this.magneticTime=0;
        this.AbsorbRadius=MinAbsorbRadius;
    },
    
    oneSecondUpdate: function () {  
        this.nowSecond+=1;
        if(this.parent.magneticState==true)
        {
            this.magneticTime+=1;
            if(this.magneticTime>AbsorbTime)
            {
                this.reduceAbsorbRadius();          
            }
        }
        //console.log(this.nowSecond,this.magneticTime);

    },

});

var SnakeBody=cc.Sprite.extend({
    prev : null,
    next : null,
    oldPosX:0,
    oldPosY:0,
    oldDirection:1,
    snakeDirection:0.78539,
    snakeSpeedB:150,
    speedUpState:false,
    radius:10,
    directionChange:false,
    _image:null,
    _master:null,

    ctor:function (image,toWhere,master) {
        this._super(image);
        this.snakeDirection= toWhere;
        this._master=master;
        this.rotation=90-this.snakeDirection*(180/Math.PI);
        this.snakeSpeedB= PlayerSpeed;
        this.radius=BeginRadius;
        this.oldPosX=this.x;
        this.oldPosY=this.y;
        this.oldDirection=this.rotation;
        this.speedUpState=false;
        //this.schedule(this.updateOldPos,0.5);
        //this.scheduleUpdate();//加上这一句就能启动update
        // precursor=p;
        // next=n;
        this._image=image;
    },
    update:function(dt)
    {

        this.rotationChange();
        this.x+=this.snakeSpeedB*dt*Math.cos(-(this.rotation-90)*(Math.PI/180));
        this.y+=this.snakeSpeedB*dt*Math.sin(-(this.rotation-90)*(Math.PI/180));

    },

    upDate:function(dt)
    {
        this.oldPosX=this.x-MinDistance*Math.cos(-(this.oldDirection-90)*(Math.PI/180));
        this.oldPosY=this.y-MinDistance*Math.sin(-(this.oldDirection-90)*(Math.PI/180));
        this.oldDirection=this.rotation;
        this.rotationChange();

        //this.x+=this.snakeSpeedB*dt*Math.cos(-(this.rotation-90)*(Math.PI/180));
        //this.y+=this.snakeSpeedB*dt*Math.sin(-(this.rotation-90)*(Math.PI/180));

        //this.x+=this.snakeSpeedB*dt*Math.cos(-(this.prev.oldDirection-90)*(Math.PI/180));
        //this.y+=this.snakeSpeedB*dt*Math.sin(-(this.prev.oldDirection-90)*(Math.PI/180));

        this.x=this.prev.oldPosX;
        this.y=this.prev.oldPosY;

        //this.rotation=this.prev.oldDirection;
        //console.log(this.prev.oldPosX,this.prev.oldPosY);
        //console.log(this.x,this.y);
    },

    rotationChange:function()
    {
        if(this.rotation>360)
        {
            this.rotation=this.rotation%360;
        }
        else if(this.rotation<0)
        {
            this.rotation=360+this.rotation;
        }

        if(Math.abs(this.prev.oldDirection-this.rotation)<PreventRotation)
            return;
        if(this.rotation<this.prev.oldDirection)
        {
            if(this.prev.oldDirection-this.rotation>180)
            {
                this.rotation-=rotationSpeed;
            }
            else
            {
                this.rotation+=rotationSpeed;
            } 
        }
        else
        {
            if(this.rotation-this.prev.oldDirection>180)
            {
                this.rotation+=rotationSpeed;
            }
            else
            {
                this.rotation-=rotationSpeed;
            }
        }
    },




});


