var ComputerAI = cc.Sprite.extend({
    normalState:true,
    huntingState:false,
    escapeState:false,
    _Master:null,

    ctor:function (Master) {
        this._super();
        this.normalState=true;
        this.huntingState=false;
        this.escapeState=false;
        this._Master=Master;
        this.scheduleUpdate();
    },

    update:function(dt)
    {
        if(this._Master.parent.gameOver==true)
        {
            return;
        }

        this.finiteAutomaton();
    },

    finiteAutomaton:function(){
        //console.log(Math.abs(this._Master.SNHead.x-PlayerBodyCollision[0][Math.floor(PlayerBodyCollision[0].length/3)].x)
        //,Math.abs(this._Master.SNHead.y-PlayerBodyCollision[0][Math.floor(PlayerBodyCollision[0].length/3)].y));

        //console.log(BarrierCollisionLR[0]-150,this._Master.SNHead.x);
        let size=cc.winSize;
        if((BarrierCollisionLR[0].x-size.width/2)-this._Master.SNHead.x<30+this._Master.SNHead.radius)
        {
            //console.log("左");
            this.leftRandomMove();
        }
        else if(this._Master.SNHead.x-(BarrierCollisionLR[1].x+size.width/2)<30+this._Master.SNHead.radius)
        {
            //console.log("右");
            this.rightRandomMove();
        }
        else if((BarrierCollisionUD[0].y-size.height/2)-this._Master.SNHead.y<30+this._Master.SNHead.radius)
        {
            //console.log("下");
            this.downRandomMove();
        }
        else if(this._Master.SNHead.y-(BarrierCollisionUD[1].y+size.height/2)<30+this._Master.SNHead.radius)
        {
            //console.log("上");
            this.upRandomMove();
        }      


        else if(PlayerHeadCollision.length>0
        &&Math.abs(this._Master.SNHead.x-PlayerHeadCollision[0].x)
        <100+this._Master.SNHead.radius+PlayerHeadCollision[0].radius
        &&Math.abs(this._Master.SNHead.y-PlayerHeadCollision[0].y)
        <100+this._Master.SNHead.radius+PlayerHeadCollision[0].radius)
        {
            //console.log("逃跑1");
            this.escapeStateLogic1();
        }
        else if(PlayerBodyCollision.length>0
        &&Math.abs(this._Master.SNHead.x-PlayerBodyCollision[0][Math.floor(PlayerBodyCollision[0].length/3)].x)
        <100+this._Master.SNHead.radius+PlayerBodyCollision[0][Math.floor(PlayerBodyCollision[0].length/3)].radius
        &&Math.abs(this._Master.SNHead.y-PlayerBodyCollision[0][Math.floor(PlayerBodyCollision[0].length/3)].y)
        <100+this._Master.SNHead.radius+PlayerBodyCollision[0][Math.floor(PlayerBodyCollision[0].length/3)].radius)
        {
            //console.log("逃跑2");
            this.escapeStateLogic2();
        }
        else if(PlayerBodyCollision.length>0
        &&Math.abs(this._Master.SNBody.x-PlayerBodyCollision[0][Math.floor(2*PlayerBodyCollision[0].length/3)].x)
        <100+this._Master.SNHead.radius+PlayerBodyCollision[0][Math.floor(2*PlayerBodyCollision[0].length/3)].radius
        &&Math.abs(this._Master.SNHead.y-PlayerBodyCollision[0][Math.floor(2*PlayerBodyCollision[0].length/3)].y)
        <100+this._Master.SNHead.radius+PlayerBodyCollision[0][Math.floor(2*PlayerBodyCollision[0].length/3)].radius)
        {
            //console.log("逃跑3");
            this.escapeStateLogic3();
        }
        else if(PlayerHeadCollision.length>0
        &&Math.abs(this._Master.SNHead.x-PlayerHeadCollision[0].x)
        <150+this._Master.SNHead.radius+PlayerHeadCollision[0].radius
        &&Math.abs(this._Master.SNHead.y-PlayerHeadCollision[0].y)
        <150+this._Master.SNHead.radius+PlayerHeadCollision[0].radius)
        {
            //console.log("追击");
            this.huntingLogic();
        }
        else
        {
            //console.log("吃豆");
            this.normalLogic();
        }


    },

    
    leftRandomMove:function(){
        this._Master.snakeDirection=Math.PI;
    },
 
    rightRandomMove:function(){
        this._Master.snakeDirection=0;
    },

    upRandomMove:function(){
        this._Master.snakeDirection=Math.PI/2;
    },

    downRandomMove:function(){
        this._Master.snakeDirection=3*Math.PI/2;
    },

    normalLogic:function(){
        let minFoodDistance=10000;
        let whichFood=null;
        for(let i=0;i<foodArray.length;++i)
        {
            let xDistance=Math.abs(this._Master.SNHead.x-foodArray[i].x);
            let yDistance=Math.abs(this._Master.SNHead.y-foodArray[i].y);
            if(xDistance<minFoodDistance
            &&yDistance<minFoodDistance)
            {
                minFoodDistance=Math.sqrt(Math.pow(xDistance,2)+Math.pow(yDistance,2));
                whichFood=foodArray[i];
            }
        }
        //console.log(foodArray);
        if(whichFood==null)
        {
            return;
        }
        this._Master.snakeDirection=Math.atan2(whichFood.y-this._Master.SNHead.y,whichFood.x-this._Master.SNHead.x);


    },

    huntingLogic:function(){
        this._Master.snakeDirection=Math.atan2(PlayerHeadCollision[0].y-this._Master.SNHead.y,PlayerHeadCollision[0].x-this._Master.SNHead.x);
    },
    escapeStateLogic1:function(){

        this._Master.snakeDirection=Math.atan2(-PlayerHeadCollision[0].y+this._Master.SNHead.y,PlayerHeadCollision[0].x-this._Master.SNHead.x);      
    },

    escapeStateLogic2:function(){

        this._Master.snakeDirection=Math.atan2(-PlayerBodyCollision[0][Math.floor(PlayerBodyCollision[0].length/3)].y+this._Master.SNHead.y,PlayerBodyCollision[0][Math.floor(PlayerBodyCollision[0].length/3)].x-this._Master.SNHead.x);      
    },

    escapeStateLogic3:function(){

        this._Master.snakeDirection=Math.atan2(-PlayerBodyCollision[0][Math.floor(2*PlayerBodyCollision[0].length/3)].y+this._Master.SNHead.y,PlayerBodyCollision[0][Math.floor(2*PlayerBodyCollision[0].length/3)].x-this._Master.SNHead.x);      
    },

    speedUp:function(){
        this._Master.speedUp();
    },

    slowDown:function(){
        this._Master.speedUp();
    }
});